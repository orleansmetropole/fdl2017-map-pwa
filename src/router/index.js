import Vue from 'vue'
import Router from 'vue-router'
import Event from '@/components/Event'
import Pratique from '@/components/Pratique'
import Global from '@/components/Global'
import Home from '@/components/Home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/event',
      name: 'Event',
      component: Event
    },
    {
      path: '/pratique',
      name: 'Pratique',
      component: Pratique
    },
    {
      path: '/global',
      name: 'Global',
      component: Global
    }
  ]
})
